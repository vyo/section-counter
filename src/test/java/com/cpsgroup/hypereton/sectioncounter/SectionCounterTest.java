package com.cpsgroup.hypereton.sectioncounter;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class SectionCounterTest {

	private final String PATH_PREFIX = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator;

	SectionCounter sectionCounter;

	@Test
	public void testFileOnly() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
			"closure descriptor", "[^b]section descriptor",
			"subsection descriptor", "file" });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testBooleanTrue() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", "true" });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testBooleanFalse() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", "false" });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testBooleanRandomValue() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", "random" });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testBooleanNull() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", null });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testRangeValid() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", "1", "2" });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testRangeInvalid() {
		try {
			sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", "1", "two" });
			fail("Should have thrown IllegalArgumentException!");
		} catch (IllegalArgumentException e){
		}
	}

	@Test
	public void testAllValid() {
		sectionCounter = new SectionCounter(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", "file", "1", "2",
				"true" });
		assertTrue(sectionCounter instanceof SectionCounter);
	}

	@Test
	public void testRunValidFile() {
		sectionCounter = new SectionCounter(new String[] {"title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", 
				PATH_PREFIX + "valid.pdf", "1", "1", "true" });
		assertTrue(sectionCounter.run());
	}

	@Test
	public void testRunInvalidFile() {
		sectionCounter = new SectionCounter(new String[] {"title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", 
				PATH_PREFIX + "invalid.pdf", "1", "1", "true" });
		assertFalse(sectionCounter.run());
	}

}
