package com.cpsgroup.hypereton.sectioncounter.decomposer;

import java.util.Arrays;

/**
 * Provides methods to allow the decomposition of a {@link String} document
 * containing legal texts into structural document levels. Document levels
 * supported are <b>text, section</b> and <b>subsection.</b><br>
 * Decomposition is done by splitting the document along descriptors specific to
 * each structural level and makes use of regular expressions, see
 * {@link java.util.regex.Pattern} for further information on syntax and usage.
 * 
 * @author Manuel Weidmann
 */
public class TextDecomposer {

	private String titleDescriptor, closureDescriptor, sectionDescriptor,
			subsectionDescriptor;

	/**
	 * Creates a new {@link TextDecomposer} to decompose a compilation of legal
	 * texts into texts, sections and subsections in accordance with the
	 * specified regular expressions.
	 * 
	 * @param titleDescriptor
	 *            A regular expression {@link String} denoting the beginning of
	 *            a legal text.
	 * @param closureDescriptor
	 *            A regular expression {@link String} denoting the closing
	 *            comments of a legal text.
	 * @param sectionDescriptor
	 *            A regular expression {@link String} denoting the beginning of
	 *            a section in a legal text.
	 * @param subsectionDescriptor
	 *            A regular expression {@link String} denoting the beginning of
	 *            a subsection in a legal text.
	 * 
	 * @author Manuel Weidmann
	 */
	public TextDecomposer(String titleDescriptor, String closureDescriptor,
			String sectionDescriptor, String subsectionDescriptor) {
		this.titleDescriptor = titleDescriptor;
		this.closureDescriptor = closureDescriptor;
		this.sectionDescriptor = sectionDescriptor;
		this.subsectionDescriptor = subsectionDescriptor;
	}

	/**
	 * Extracts atomic legal texts from a {@link String} containing a legal text
	 * compilation.
	 * 
	 * @param legalTextCompilation
	 *            An HTML-formatted {@link String} containing a compilation of
	 *            legal texts.
	 * @return A {@link String} array of those legal texts that match the
	 *         specified descriptors.<br>
	 *         <b>Note:</b> Returns an empty {@link String} array if no matches
	 *         were found.
	 * 
	 * @author Manuel Weidmann
	 */
	public String[] getLegalTexts(String legalTextCompilation) {

		/**
		 * Split the text compilation along the title descriptor. With exclusion
		 * of the first element every element of the array is representing the
		 * beginning of a body of text matching the descriptor. Note that there
		 * is no such guarantee for the end of those bodies of text, since only
		 * the presence of a matching title is being detected but not the
		 * presence of a non-matching one.
		 */
		String[] legalTexts = legalTextCompilation.split(titleDescriptor);

		/**
		 * To address the aforementioned problem the resulting bodies of text
		 * are split along the closure descriptor. Since every body of text is
		 * containing only a single relevant legal text ignoring all but the
		 * first part of the document is perfectly fine. The other parts are
		 * either unrelated artifacts or ghost texts that do not match the title
		 * descriptor and are thus not relevant to the search.
		 */
		for (String legalText : legalTexts) {
			legalText = legalText.split(closureDescriptor)[0];
		}

		/**
		 * The first element of the array is not preceded by a match of the
		 * title descriptor and thus not part of a legal text and is
		 * consequently discarded. Return an empty string array if no matches
		 * were found.
		 */
		if (legalTexts.length == 1) {
			return new String[0];
		} else {
			return Arrays.copyOfRange(legalTexts, 1, legalTexts.length);
		}
	}

	/**
	 * Extracts the sections from a given legal text.
	 * 
	 * @param legalText
	 *            An HTML-formatted {@link String} containing a single legal
	 *            text.
	 * @return A {@link String} array containing all the sections of the legal
	 *         text.<br>
	 *         <b>Note:</b> Returns a {@link String} array that contains the
	 *         input text as only entry if no matches were found.
	 * 
	 * @author Manuel Weidmann
	 */
	public String[] getSections(String legalText) {

		/**
		 * Split the legal text along the section descriptor.
		 */
		String[] sections = legalText.split(sectionDescriptor);

		/**
		 * The first element of the array is not preceded by a match of the
		 * section descriptor and thus not a section and is consequently
		 * discarded. If no matches were found return the original legal text as
		 * a single entry in a string array.
		 */
		if (sections.length == 1) {
			return new String[] { legalText };
		} else {
			return Arrays.copyOfRange(sections, 1, sections.length);
		}
	}

	/**
	 * Extracts the subsections from a given section.
	 * 
	 * @param legalTextSection
	 *            An HTML-formatted {@link String} containing a single legal
	 *            text section.
	 * @return A {@link String} array containing all the subsections of the
	 *         given legal text section.<br>
	 *         <b>Note:</b> Returns a {@link String} array that contains the
	 *         input section as the only entry if no matches were found.
	 * 
	 * @author Manuel Weidmann
	 */
	public String[] getSubsections(String legalTextSection) {

		String[] subsections = legalTextSection.split(subsectionDescriptor);

		/**
		 * The first element of the array is not preceded by a match of the
		 * subsection descriptor and thus not a subsection and is consequently
		 * discarded. If no matches were found return the original section as a
		 * single string entry in a string array.
		 */
		if (subsections.length == 1) {
			return new String[] { legalTextSection };
		} else {
			return Arrays.copyOfRange(subsections, 1, subsections.length);
		}
	}

	/**
	 * Breaks down a document containing legal texts into structural levels laid
	 * out as specified by the descriptors in the constructor.
	 * 
	 * @param legalTextCompilation
	 *            An HTML-formatted {@link String} containing a compilation of
	 *            legal texts.
	 * @return A jagged array containing the original text broken down into
	 *         {@link String}s.
	 * 
	 * @author Manuel Weidmann
	 */
	public String[][][] getDecomposedText(String legalTextCompilation) {

		/**
		 * Split legal text compilation into legal texts.
		 */
		String[] legalTexts = getLegalTexts(legalTextCompilation);

		/**
		 * Split legal texts into sections.
		 */
		String[][] sections = new String[legalTexts.length][];
		for (int i = 0; i < legalTexts.length; i++) {
			sections[i] = getSections(legalTexts[i]);
		}

		/**
		 * Split sections into subsections.
		 */
		String[][][] subsections = new String[sections.length][][];
		for (int i = 0; i < sections.length; i++) {
			subsections[i] = new String[sections[i].length][];
			for (int j = 0; j < sections[i].length; j++) {
				subsections[i][j] = getSubsections(sections[i][j]);
			}
		}

		/**
		 * Return the decomposed document.
		 */
		return subsections;
	}
}
